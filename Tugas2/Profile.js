import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image,
} from 'react-native';
let tinggi = Dimensions.get('window').height;
let lebar = Dimensions.get('window').width;
const componentName = () => {
  return (
    <View style={styles.container}>
      <View style={styles.photo}>
        <Image
          style={styles.photoprofile}
          source={{
            uri: 'https://reactnative.dev/img/tiny_logo.png',
          }}
        />
        <Text style={styles.textphoto}>Salman Alfarisi</Text>
      </View>
      <View style={styles.profilecontent}>
        <View style={styles.contentdalam}>
          <Text style={{paddingLeft: 10}}>Tanggal Lahir</Text>
          <Text style={{paddingRight: 10}}>21 Juni 1997</Text>
        </View>
        <View style={styles.contentdalam}>
          <Text style={{paddingLeft: 10}}>Jenis Kelamin</Text>
          <Text style={{paddingRight: 10}}>Laki-Laki</Text>
        </View>
        <View style={styles.contentdalam}>
          <Text style={{paddingLeft: 10}}>Hobi</Text>
          <Text style={{paddingRight: 10}}>Ngoding</Text>
        </View>
        <View style={styles.contentdalam}>
          <Text style={{paddingLeft: 10}}>No Telp.</Text>
          <Text style={{paddingRight: 10}}>082123456789</Text>
        </View>
        <View style={styles.contentdalam}>
          <Text style={{paddingLeft: 10}}>Email</Text>
          <Text style={{paddingRight: 10}}>Jhon.doe@gmail.com</Text>
        </View>
      </View>
    </View>
  );
};
export default componentName;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  photo: {
    height: tinggi * 0.3,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3EC6FF',
  },
  photoprofile: {
    width: 100,
    height: 100,
    borderRadius: 75,
    backgroundColor: 'red',
  },
  textphoto: {
    color: 'white',
    fontWeight: 'bold',
    paddingTop: 20,
  },
  profilecontent: {
    flex: 0.5,
    marginTop: -20,
    marginHorizontal: 25,
    elevation: 4,
    borderRadius: 10,
    shadowColor: '#000',
    backgroundColor: '#fff',
  },
  contentdalam: {
    flex: 0.3,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
