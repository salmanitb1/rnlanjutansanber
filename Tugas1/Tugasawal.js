import React from "react";
import { 
    View,
    Text,
    StyleSheet
} from "react-native";

const Tugasawal = (props) => (
    <View style={styles.container}>
        <Text>Hello Kelas React Native Lanjutan Sanbercode!</Text>
    </View>
    )
export default Tugasawal;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});